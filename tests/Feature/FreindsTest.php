<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;

class FriendsTest extends TestCase
{
    /** @var \App\User */
    protected $user;

    public function setUp()
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    /** @test */
    public function get_friends()
    {
        $this->actingAs($this->user)
            ->getJson('/api/friends')
            ->assertSuccessful()
            ->assertJsonStructure(['total', 'per_page', 'current_page', 'last_page', 'first_page_url', 'last_page_url', 'next_page_url', 'prev_page_url', 'path', 'from', 'to', 'data']);
    }
    /*public function testFriends($object)
    {
        $object->assertJsonStructure(['id', 'name', 'friendship_id']);
    }

    public function provider()
    {
        $result = $this->actingAs($this->user)
            ->getJson('/api/friends');

        $result->assertSuccessful()
            ->assertJsonStructure(['total', 'per_page', 'current_page', 'last_page', 'first_page_url', 'last_page_url', 'next_page_url', 'prev_page_url', 'path', 'from', 'to', 'data']);
        return $result->data;
    }*/
}
