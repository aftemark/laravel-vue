<?php

namespace App\Http\Controllers;

use App\Friendship;
use Illuminate\Http\Request;

class FriendshipController extends Controller
{
    public function friends(Request $request)
    {
        return $this->getFriendships($request, true);
    }

    public function pending(Request $request)
    {
        return $this->getFriendships($request, false);
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'friendship_id' => 'required|exists:friendships,id'
        ]);

        $user = $request->user();
        $friendship = Friendship::findOrFail($request->friendship_id);
        
        if ($friendship->user_id != $user->id && $friendship->sender_id != $user->id)
            return response(402);

        if (!$friendship->approved) {
            $friendship->approved = true;
            $friendship->save();
        }
    }

    protected function getFriendships(Request $request, bool $getFriends)
    {
        $user = $request->user();
        $friendships = Friendship::where(function($q) use ($user) {
            $q->where('sender_id', $user->id)
                ->orWhere('user_id', $user->id);
        })->whereApproved($getFriends)->with('user', 'sender')->paginate(1);

        $friendships->transform(function ($friendship) use ($user) {
            $return = $friendship->sender_id == $user->id ? $friendship->user : $friendship->sender;
            $return['friendship_id'] = $friendship->id;
            return $return;
        });

        return $friendships;
    }
}
